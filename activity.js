
db.fruits.aggregate([
            { $unwind : "$origin" },
            { $group : { _id : "$origin" , fruits : { $sum : 1 } } },
                        {$project: {name: 0}}
        ])




db.fruits.aggregate([
    { $match: { onSale: true } },
    { $count: "fruitsOnSale" }
])


db.fruits.aggregate([
    { $match: { stock: { $gte: 20 } } },
    { $count: "enoughStock" }
])



db.fruits.aggregate([
    {$match: { onSale: true } },
    { $group: {_id: "$supplier_id", avg_price: {$avg: "$price"} } }
])



db.fruits.aggregate([
    
    { $group: {_id: "$supplier_id", max_price: {$max: "$price"} } }
])


db.fruits.aggregate([
    
    { $group: {_id: "$supplier_id", min_price: {$min: "$price"} } }
])